<?php
	
	function create_table($data,$a="",$b=""){
		echo '<table class="'.$a.'" '.$b.'>';
		set_heading_table($data['head']);
		set_body_table($data['body']);
		echo '</table>';
	}
	function set_heading_table($data){
		if($data!=NULL){
			$heading='<thead>';
			$heading.='<tr>';
			foreach ($data as $row) {
				if(is_array($row)){
					$heading.="<td ".$row[1].">".$row[0]."</td>";

				} 
				else{
					$heading.="<td>".$row."</td>";			
				}

			}
			$heading.='</tr>';
			$heading.='</thead>';
			echo $heading;
		}	
	}
	function set_body_table($data){
		$body='<tbody>';
		if($data!=NULL){
			foreach ($data as $row) {
				$body.='<tr>';
				for ($i=0; $i < count($row); $i++) {
					if(is_array($row[$i])){
						$body.="<td ".$row[$i][1].">".$row[$i][0]."</td>";				
					} 
					else{
						$body.="<td>".$row[$i]."</td>";				
					}
				}
				$body.='</tr>';
			}
		}
		
		$body.='</tbody>';
		echo $body;
		
	}

?>