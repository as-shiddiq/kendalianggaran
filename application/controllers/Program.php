<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Program extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_program');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_program->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','kode program','nama program','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->kode_program,
													$row->nama_program,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('program?ubah&id='.$row->id_program).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Program"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('program/hapus?id='.$row->id_program).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Program';
		$data['body']=$this->load->view('v_program',$databody,true);
		$data['js']=$this->load->view('js/js_program',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_program->insert($data);
		}
		redirect('program');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_program=$this->input->post('id_program');
			$where=array('id_program'=>$id_program);
			$this->m_program->update($data,$where);
		}
		redirect('program');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_program' => $this->input->get('id'),
		);
		$this->m_program->delete($where);
		redirect('program');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_program = $this->input->post('id_program');
		$kode_program = $this->input->post('kode_program');
		$nama_program = $this->input->post('nama_program');
		$tanggal_program = date('Y-m-d');
		$data=array(
			'id_program' => $id_program,
			'kode_program' => $kode_program,
			'nama_program' => $nama_program,
			'tanggal_program' => $tanggal_program,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:24:45  **/
/**************************************/
