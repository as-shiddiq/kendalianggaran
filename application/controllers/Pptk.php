<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pptk extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_pptk');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$id_ta=id_ta();
		$get_data=$this->m_pptk->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nip pegawai','nama','nama bidang','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nip_pegawai,
													$row->nama_pegawai,
													$row->nama_bidang,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('pptk?ubah&id='.$row->id_pptk).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data PPTK"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('pptk/hapus?id='.$row->id_pptk).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$databody['id_ta']=$id_ta;
		$data['title']='Data PPTK';
		$data['body']=$this->load->view('v_pptk',$databody,true);
		$data['js']=$this->load->view('js/js_pptk',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_pptk->insert($data);
		}
		redirect('pptk');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_pptk=$this->input->post('id_pptk');
			$where=array('id_pptk'=>$id_pptk);
			$this->m_pptk->update($data,$where);
		}
		redirect('pptk');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_pptk' => $this->input->get('id'),
		);
		$this->m_pptk->delete($where);
		redirect('pptk');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_pptk = $this->input->post('id_pptk');
		$id_pegawai = $this->input->post('id_pegawai');
		$id_bidang = $this->input->post('id_bidang');
		$id_ta = $this->input->post('id_ta');
		$jenis_pptk = $this->input->post('jenis_pptk');
		$tanggal_pptk = date('Y-m-d');
		$data=array(
			'id_pptk' => $id_pptk,
			'id_pegawai' => $id_pegawai,
			'id_bidang' => $id_bidang,
			'id_ta' => $id_ta,
			'jenis_pptk' => $jenis_pptk,
			'tanggal_pptk' => $tanggal_pptk,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:23:05:23  **/
/**************************************/
