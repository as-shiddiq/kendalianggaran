<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
 {
    parent::__construct();
    	$this->load->model('m_kegiatan');
			$this->load->model('m_anggaran');
			$this->load->model('m_pptk');
		$this->load->model('m_realisasi');

	}

	public function index(){
		$id_ta = id_ta();
		$id_pegawai=$this->input->get('id_pegawai');
		if($id_pegawai!=''){
			$this->session->set_userdata('id_pegawai',$id_pegawai);
		}
		elseif($this->session->userdata('id_pegawai')!=''){
			$id_pegawai=$this->session->userdata('id_pegawai');
		}
		$this->db->where('a.id_pegawai',$id_pegawai);
		$this->db->group_by('kode_kegiatan');
		$get_data=$this->m_anggaran->get_data();

		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','kode rekening','nama kegiatan','program','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->kode_program.'. '.$row->kode_kegiatan,
													$row->nama_kegiatan,
													$row->nama_program,
													array('data'=>'<div class="btn-group">
													<!-- <a target="_blank" href="'.site_url('laporan/cetakdata/'.$row->id_kegiatan).'" class="btn btn-success btn-xs" onclick="return confirm(\'Cetak detail realisasi?\');"><i class="fa fa-print"></i> Cetak</a>-->
                        	<a target="_blank" href="'.site_url('laporan/cetakrekap/'.$row->id_kegiatan).'" class="btn btn-warning btn-xs" onclick="return confirm(\'Cetak rekap realisasi?\');"><i class="fa fa-print"></i> Cetak</a>
                        </div>','width'=>'100px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$databody['id_pegawai']=$id_pegawai;
		$databody['id_ta']=$id_ta;
		$data['title']='Data Kegiatan';
		$data['body']=$this->load->view('v_laporan',$databody,true);
		$data['js']=$this->load->view('js/js_kegiatan',$databody,true);
		$this->load->view('html/html',$data);
	}

	function cetakdata()
    {
		$id_kegiatan = $this->uri->segment(3);

		$this->data['datahead'] = $this->db
								  ->select('p.*, u.*, s.nama_pegawai, s.nip_pegawai, t.id_anggaran, u.id_program, s.id_pegawai')
								  ->where('p.id_kegiatan', $id_kegiatan)
								  ->join('tb_program u', 'p.id_program = u.id_program', 'LEFT')
								  ->join('tb_anggaran r', 'p.id_kegiatan = r.id_kegiatan', 'LEFT')
								  ->join('tb_realisasi t', 'r.id_anggaran = t.id_anggaran', 'LEFT')
								  ->join('tb_pegawai s', 'r.id_pegawai = s.id_pegawai', 'LEFT')
								  ->get('tb_kegiatan p', $this->uri->segment(4))->row_array();


        $this->data['datakegiatan'] = $this->m_kegiatan->printkegiatan();
        $this->load->view('laporan/v_cetakdokumen', $this->data);
		// print_r($this->data['datakegiatan']);

        $pdf = $this->output->get_output();
        generate_pdf($pdf,$id_kegiatan.'-'.date('YmdHis'),'A4');
    }

		function cetakrekap($id_kegiatan='')
	    {
					$id_ta=id_ta();
					$this->db->where('a.id_kegiatan',$id_kegiatan);
					$this->db->where('a.id_ta',$id_ta);
					$data['datahead']=$this->m_anggaran->get_data()->row_array();

					$this->db->where('a.id_kegiatan',$id_kegiatan);
					$this->db->where('a.id_ta',$id_ta);
					$data['datakegiatan'] = $this->m_anggaran->get_data()->result();
	        $data['id_kegiatan'] = $id_kegiatan;
	        $this->load->view('laporan/v_rekaprealisasi', $data);

	        $pdf = $this->output->get_output();
	        generate_pdf($pdf,$id_kegiatan.'-'.date('YmdHis'),'A4');
	    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
