<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_ta extends CI_Model {
	function get_data(){
			$data=$this->db->select('*')
						->from('tb_ta')
						->order_by('id_ta','DESC')
						->get();
					return $data;
	}
	function insert($data){
		$this->db->insert('tb_ta',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_ta',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_ta',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_ta',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}
	function aktif($where){
		$this->db->where($where);
		$this->db->update('tb_ta',array('aktif'=>'Y'));
		$get=$this->db->get_where('tb_ta',$where)->row();
		$nama_ta=$get->nama_ta;
		$this->session->set_flashdata('info',info_success(icon('check').' Tahun Anggaran '.$nama_ta.' telah diaktifkan'));
	}
}
