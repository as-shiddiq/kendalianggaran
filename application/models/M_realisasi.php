<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_realisasi extends CI_Model {
	function get_data(){
				$data=$this->db->select('a.*,ba.kode_belanja,ba.nama_belanja,c.nama_ta,d.nama as nama_pengguna')
						->from('tb_realisasi a')
						->join('tb_anggaran b','a.id_anggaran=b.id_anggaran','left')
						->join('tb_ta c','a.id_ta=c.id_ta','left')
						->join('tb_pengguna d','a.id_pengguna=d.id_pengguna','left')
						->join('tb_belanja ba','b.id_belanja=ba.id_belanja','left')
						->order_by('id_realisasi','DESC')
						->get();
				return $data;
	}
	function get_realisasi($fetch=true,$status='Realisasi'){
					$data=$this->db->select('IFNULL(SUM(realisasi),0) total_realisasi')
							->from('tb_realisasi a')
							->join('tb_anggaran b','a.id_anggaran=b.id_anggaran','left')
							->join('tb_pegawai ba','b.id_pegawai=ba.id_pegawai','left')
							->join('tb_pptk baa','ba.id_pegawai=baa.id_pegawai','left')
							->order_by('id_realisasi','DESC')
							->where('b.id_ta = baa.id_ta')
							->where('a.status = "'.$status.'"')
							->get();
					if($fetch==true){
						$r=$data->row();
						return $r->total_realisasi;
					}
					else{
						return $data;
					}
		}
	function insert($data){
		$this->db->insert('tb_realisasi',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_realisasi',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_realisasi',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_realisasi',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}

	public function printkegiatan(){

		$sql = "SELECT * FROM tb_kegiatan
				LEFT JOIN tb_anggaran ON tb_kegiatan.id_kegiatan = tb_anggaran.id_kegiatan
				LEFT JOIN tb_program ON tb_kegiatan.id_program = tb_program.id_program
				LEFT JOIN tb_realisasi ON tb_anggaran.id_anggaran = tb_realisasi.id_anggaran
				LEFT JOIN tb_belanja ON tb_anggaran.id_belanja = tb_belanja.id_belanja
				WHERE kode_kegiatan =  kode_kegiatan" ;
		$query = $this->db->query($sql);

		if($query->num_rows()>0)
			{
			return $query-> result_array();
		}else{
			return array();
		}
	}
}
