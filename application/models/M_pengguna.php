<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pengguna extends CI_Model {
	function get_data(){
				$data=$this->db->select('a.*,b.nip_pegawai,b.nama_pegawai,c.nama_bidang')
						->from('tb_pengguna a')
						->join('tb_pegawai b','a.id_pegawai=b.id_pegawai','left')
						->join('tb_bidang c','a.id_bidang=c.id_bidang','left')
						->order_by('id_pengguna','DESC')
						->get();
				return $data;
	}
	function insert($data){
		$this->db->insert('tb_pengguna',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_pengguna',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_pengguna',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_pengguna',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}
}
