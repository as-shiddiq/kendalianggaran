<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Aktivitas Realisasi <small>pada tahun anggaran <?=nama_ta()?></small></h3>
                </div>
                <div class="col-md-6">
                    <div id="reportrange" class="pull-right" >
                        <?php
                            $op=null;
                            $op['semua']='semua';
                            for ($i=1; $i <13 ; $i++) {
                              if(strlen($i)==1){
                                $i='0'.$i;
                              }
                              $op[$i]=bulan_huruf($i).' '.nama_ta();
                            }
                            echo select('bulangrafik',$op,'');

                         ?>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="load-grafik">

                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                <div class="x_title">
                    <h2>Total Realisasi Perbidang</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-6">
                  <?php
                      $this->db->where('a.id_ta',$id_ta);
                      $this->db->group_by('nama_bidang');
                      $ambilbidang=$this->m_pptk->get_data();
                      foreach ($ambilbidang->result() as $rowbidang) {
                        $id_bidang=$rowbidang->id_bidang;
                        $this->db->where('a.id_ta',$id_ta);
                        $this->db->where('baa.id_bidang',$id_bidang);
                        $realisasi=$this->m_realisasi->get_realisasi();


                        $this->db->where('a.id_ta',$id_ta);
                        $this->db->where('baa.id_bidang',$id_bidang);
                        $anggaran=$this->m_anggaran->get_pagu();
                        if($anggaran==0){
                            $persentase=0;
                        }
                        else{
                          $persentase=($realisasi/$anggaran)*100;
                        }
                        ?>
                        <div>
                            <p><?=$rowbidang->nama_bidang?></p>
                            <div class="">
                                <div class="progress progress_md" style="width: 100%;">
                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?=number_format($persentase,2)?>"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                      }
                   ?>

                 </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

  </div>
<br />
