<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Program</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_program' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_program',$where)->row();
                $id_program = $row->id_program;
								$kode_program = $row->kode_program;
								$nama_program = $row->nama_program;
								$tanggal_program = $row->tanggal_program;
              }
              else{
                $parameter='tambah';
                $id_program = '';
								$kode_program = '';
								$nama_program = '';
								$tanggal_program = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>program/<?=$parameter?>">
						<?php echo input_hidden('id_program',$id_program,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>kode program</label>
								<?php echo input_text('kode_program',$kode_program,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-6 full">
							<div class="form-group">
								<label>nama program</label>
								<?php echo textarea('nama_program',$nama_program,'md-input','required');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Program</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>program?tambah" class="btn btn-success" ns-click="true" ns-title="Data Program"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
