<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <?php include '_style.php';?>

  </head>
  <body>
    <h3>Ini Contoh Cetak</h3>

    <table id="table">
      <thead>
        <tr>
          <th width="20px">No</th>
          <th>Nama</th>
          <th>Alamat</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="center">1.</td>
          <td>Mantap</td>
          <td>Jl. Anjir</td>
        </tr>
        <tr>
          <td class="center">2.</td>
          <td>Mantap</td>
          <td>Jl. Anjir</td>
        </tr>
        <tr>
          <td class="center">3.</td>
          <td>Mantap</td>
          <td>Jl. Anjir</td>
        </tr>
        <tr>
          <td class="center">4.</td>
          <td>Mantap</td>
          <td>Jl. Anjir</td>
        </tr>
      </tbody>
    </table>

  </body>
</html>
