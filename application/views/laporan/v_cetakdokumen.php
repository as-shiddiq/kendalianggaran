<table class="tables" style="text-transform:uppercase;font-weight:bold;">
   <tr>
     <td width="150px">PROGRAM</td>
     <td width="10px">:</td>
     <td width="400px"><?=$datahead['nama_program'] ?></td>
   </tr>
    <tr>
      <td>KEGIATAN</td>
      <td>:</td>
      <td width="400px"><?=$datahead['nama_kegiatan'] ?></td>
    </tr>
     <tr>
       <td>KODE KEGIATAN</td>
       <td>:</td>
       <td width="400px"><?=$datahead['kode_kegiatan'] ?></td>
     </tr>
      <tr>
       <td>PPTK</td>
       <td>:</td>
       <td width="400px"><?=$datahead['nama_pegawai'] ?></td>
     </tr>
     <tr>
       <td>DICETAK PADA</td>
       <td>:</td>
       <td width="400px"><?php echo strtoupper(indo_date(date('Y-m-d'))); ?></td>
     </tr>
</table>
<hr><style type=""></style><br>
<style media="screen">
       body{
         font-size: 13px;
         font-family: helvetica;
         margin: 0 30px

       }
     .bg-blue-light{
      background: ;
      color: black
      /*background: #89d;
      color: #fff*/
     }
     .text-center{
       text-align: center;
     }
     .table-me{
       border-spacing: 0;
       font-size: 11px;
       width: 100%
     }
     .table-me tr td:nth-child(1){
       text-align: center;
     }
     .table-me thead tr td{
       background: grey;
       color: #fff;
       padding: 10px 2px
     }
     .table-me tr td{
       vertical-align: top;
       padding-left:4px
     }
     .table-me tr th,
     .table-me tr td{
       border-left:1px solid #888;
       border-top:1px solid #888
     }
     .table-me tr th:last-child,
     .table-me tr td:last-child{
       border-right: 1px solid #888
     }
     .table-me tr:last-child th,
     .table-me tr:last-child td{
       border-bottom: 1px solid #888
     }
     .table-me tbody tr td{
       padding: 5px 4px
     }
     .style-header{
       font-size: 16px
     }
     .fc-red{
       color: #a00
     }
     .bg-red,
     .bg-red tr td{
       background-color: #fbb;
     }
     .isi{
       text-align: justify;
       line-height: 20px
     }
     .text-center{
       text-align: center;
     }
     h2{
       text-align: center;
       text-decoration: underline;
       margin: 0;
       font-size: 16px;
       padding: 0
     }
     .isi span{
       margin-left: 30px
     }
     .listnama{
       margin-left: 30px;
       margin-top: 10px;
       margin-bottom: 10px
     }
     .break{
       page-break-after: always;
     }
   </style>
<?php $anggaran=$this->m_anggaran->get_pagu(); ?>
<table class="table-me">
  <thead bgcolor="bg-grey">
  <tr style="background:#94958d; color:white; text:bold">
      <td align="center">NO</td>
      <td align="center">KODE REKENING</td>
      <td align="center">URAIAN</td>
      <td align="center">PAGU ANGGARAN</td>
      <td align="center">REALISASI</td>
      <td align="center">DANA YANG DIMINTA</td>
      <td align="center">SISA ANGGARAN</td>
      <td align="center">TANGGAL</td>

    </tr>

  </thead>
  <tbody border="0">
    <?php
    $this->db->where('a.id_kegiatan',$this->uri->segment(3));
    $no=0; foreach($datakegiatan as $row){ $no++ ?>
    <?php $sisa = $row['pagu_anggaran']-$row['realisasi'] ?>
    <tr>
      <td><?php echo $no; ?></td>
      <td><?php echo $row['kode_kegiatan']; ?></td>
      <td><?php echo $row['nama_belanja']; ?></td>
      <td><?php echo uangindonesia($row['pagu_anggaran'],'Rp.'); ?></td>
      <td><?php if ($row['realisasi'] == NULL) {
        echo "<i>-</i>";
      } else {
        echo uangindonesia($row['realisasi'],'Rp.');
        } ?></td>
      <td>Dana yang diminta</td>
      <td>Rp. <?=uangindonesia($sisa)?></td>
      <td><?=$row['tanggal_realisasi']?></td>

    </tr>
    <?php } ?>
  </tbody>

</table>
<table border="0" align="right">
<tr>
  <td width="255"><center><br><br><br><br><br>
    Pelaihari, <?php echo indo_date(date('Y-m-d')); ?> <br>
    PEJABAT PELAKSANA TEKNIS<br>

    <br><br><br><br><br><br>
    <b><u><?=$datahead['nama_pegawai'] ?></u><br></b>
    NIP. <?=$datahead['nip_pegawai'] ?>
    </center>
  </td>

</tr>
</table>
<table border="0" align="left">
<tr>
  <td width="255"><center><br><br><br><br><br><br>
    PENGGUNA ANGGARAN<br>

    <br><br><br><br><br><br>
    <b><u><?=$datahead['nama_pegawai'] ?></u><br></b>
    NIP. <?=$datahead['nip_pegawai'] ?>
    </center>
  </td>

</tr>
</table>
