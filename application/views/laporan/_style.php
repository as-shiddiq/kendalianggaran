<style media="screen">
   table{
     width: 100%;
     border-spacing: 0;
     font-family: 'roboto consended';
     font-size: 14px;
     margin-bottom: 10px
   }
  #table{
    width: 100%;
    font-family: 'roboto consended';
    font-size:14px;
    font-weight: normal;

  }
  #table{
    border-spacing: 0;
  }
  #table thead tr th{
    background: #00a65a;
    color: #fff;
  }
  #table tr th,
  #table tr td{
    border-right:1px solid #000;
    border-bottom: 1px solid #000;
    padding: 2px 4px
  }

  #table tr td{
    vertical-align: top
  }
  #table tr th:first-child,
  #table tr td:first-child{
    border-left: 1px solid #000;
  }
  #table tr:first-child th,
  #table tr:first-child td{
    border-top: 1px solid #000;
  }
  #table tfoot tr th{
    border-top: 0
  }
  .table-realisasi{
    font-size: 11px !important
  }
  .no-border-left{
    border-left: 0 !important
  }
  .no-border-bottom{
    border-bottom: 0 !important
  }
  .no-border-top{
    border-top: 0 !important
  }
  .center,
  .center td,
  .center th,
  .text-center,
  .text-center td,
  .text-center th{
    text-align: center;
  }
  .right,
  .right td,
  .right th,
  .text-right,
  .text-right td,
  .text-right th{
    text-align: right;
  }
  .bg-yellow-light{
    background: #ff8 !important;
    color: #000 !important
  }
  .bg-blue{
    background: #0073b7 !important;
    color: #fff
  }
  hr{
    border: 0;
    border-bottom: 1px solid #000;
    padding: 0;
    margin: 10px 0;
    display: block;
  }
  .dotted{
    border-bottom: 1px dotted #000 !important;
  }
  .row{
    width: 100%;
    display: block;
    clear: both;
  }
  .footnote{
    font-size: 10px ;
    display: block;
    color: #888;
    padding: 10px 0 0;
  }
  .header{
    font-family: 'roboto consended' !important;
    font-size: 25px;
    border-bottom: 3px solid #000;
    width: auto;
    display: inline;
    margin-bottom: 0px
  }

  .kop-mini{
    border-spacing: 0;
    margin-top: -40px
  }
  .kop-mini img{
    width: 20px;
    margin-top: 5px;
    margin-right: 10px
  }
  .kop-mini h1{
    font-size: 10px;
    padding: 0;
    margin: 0 0 -10px 0;
    color: #888;
    font-family: 'roboto consended' !important
  }
  .kop-mini h2{
    font-size: 12px;
    color: #888;
    padding: 0;
    margin: -10px 0 -6px ;
    font-family: 'roboto consended' !important
  }
  .kop-mini p{
    font-size: 7px;
    color: #888;
    padding: 0;
    margin: -10px 0 0 ;
    font-family: 'roboto consended' !important
  }
  h4{
    font-family: 'roboto consended';
    padding: 0;
    margin: 0
  }
  h3.heading{
    font-family: 'roboto consended';
    padding: 0 !important;
    text-align: center;
    margin: -10px 0 -10px !important;
  }
  h3.heading:nth-child(4){
    margin-bottom: 30px !important;
  }
  h3.heading2{
    font-family: 'roboto consended';
    padding: 0 !important;
    text-align: center;
    margin: -10px 0 -10px !important;
  }
  h3.heading2:nth-child(3){
    margin-bottom: 30px !important;
  }
  h5{
    padding: 0;
    margin: 0
  }
  .list-inline-me:before{
    content: "-";
  }
  .no-content-before:before{
    content: "" !important
  }
  .list-inline-me{
    font-family: 'roboto consended';
    list-style: none;
    padding: 0;
    display: block;
    margin: 0px 0 0px 0;
    /*border: 1px solid #000*/
  }
  .list-inline-me li{
    display: inline;
    padding: 0;
    margin: 0
  }
  .list-inline-me li:after{
    content: "/";
  }
  .list-inline-me li:last-child:after{
    content: "";
  }
  .bidangdak tr td:first-child{
    padding-left: 45px !important
  }
  .bidangdak tr td:last-child,
  .bidangdak tfoot tr th:last-child{
    text-align: right;
  }
  .break{
    page-break-after: always;
  }
  .cover-lampiran{
    font-size: 60px;
    font-family: 'roboto consended';
    text-align: center;
    margin-top: 25%
  }
  .heading-cover{
    font-size: 35px;
    font-family: 'roboto consended';
    text-align: center;
    padding: 0;
    margin: 0;
    margin-top: -20px;
    margin-bottom: -20px;
    color: #fff;
    box-shadow: 10px 10px 10px #000
  }
  .footer-cover{
    font-size: 26px;
    font-family: 'roboto consended';
    text-align: center;
    padding: 0;
    margin: 0;
    margin-top: -20px;
    margin-bottom: -20px
  }
  .bg-yellow,
  .bg-yellow tr td,
  .bg-yellow tr th{
    background: #ff2
  }
</style>
