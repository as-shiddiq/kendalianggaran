<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Pengguna</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Pengguna</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_pengguna' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_pengguna',$where)->row();
                $id_pengguna = $row->id_pengguna;
								$username = $row->username;
								$password = $row->password;
								$nama = $row->nama;
                $id_bidang = $row->id_bidang;
								$id_pegawai = $row->id_pegawai;
								$level = $row->level;
              }
              else{
                $parameter='tambah';
                $id_pengguna = '';
								$username = '';
								$password = '';
								$nama = '';
                $id_bidang = '';
								$id_pegawai = '';
								$level = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>pengguna/<?=$parameter?>">
						<?php echo input_hidden('id_pengguna',$id_pengguna,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>username</label>
								<?php echo input_text('username',$username,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>password</label>
								<?php echo input_text('password',$password,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nama</label>
								<?php echo input_text('nama',$nama,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>pegawai</label>
								<?php
									$op=NULL;
									$op['']='Tidak ada';
									$this->db->order_by('nip_pegawai','ASC');
									$data=$this->db->get('tb_pegawai');
									foreach($data->result() as $row){
										$op[$row->id_pegawai]=$row->nip_pegawai.' - '.$row->nama_pegawai;
									}
									echo select('id_pegawai',$op,$id_pegawai,'','data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>bidang</label>
								<?php
									$op=NULL;
									$op['']='Tidak ada';
									$data=$this->db->get('tb_bidang');
									foreach($data->result() as $row){
										$op[$row->id_bidang]=$row->nama_bidang;
									}
									echo select('id_bidang',$op,$id_bidang,'','data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>level</label>
                <?php
									$op=NULL;
                  $op['']='Pilih Salah Satu';
                  $op['0']='Administrator';
                  $op['1']='Bendahara';
                  $op['2']='PPTK';
                  $op['3']='Bidang';
									echo select('level',$op,$level,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Pengguna</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Pengguna</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>pengguna?tambah" class="btn btn-success" ns-click="true" ns-title="Data Pengguna"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
