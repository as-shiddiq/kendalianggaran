<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title?></title>
    <?php include 'head.php';?>
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
            <?php include 'menu.php';?>
            <?php include 'header.php';?>
            <?=$body?>
            <?php include 'footer.php';?>


      </div>
    </div>
    <div class="wrapper">
      <div class="content-wrapper">
        <section class="content">
        </section>
      </div>
      <?php include 'js.php';?>
      <?php
        if(isset($js)){
          echo $js;
        }
      ?>
      </div>
  </body>
</html>
