<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="index.html" class="site_title"><i class="fa fa-money"></i> <span>KA<?=nama_ta()?></span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="<?=assets()?>images/logo-profil-green.png" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Adminstrator</span>
        <h2>Online</h2>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Menu Utama</h3>

        <ul class="nav side-menu">
          <li><a href="<?=site_url()?>"><i class="fa fa-home"></i>Beranda</a></li>
          <?php if(level()==0){ ?>
          <li><a><i class="fa fa-file"></i> Data Master <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?=site_url('pegawai')?>">Data Pegawai</a></li>
              <li><a href="<?=site_url('program')?>">Data Program</a></li>
              <li><a href="<?=site_url('kegiatan')?>">Data Kegiatan</a></li>
              <li><a href="<?=site_url('belanja')?>">Data Belanja</a></li>
              <li><a href="<?=site_url('bidang')?>">Data Bidang</a></li>
              <li><a href="<?=site_url('pengguna')?>">Data Pengguna</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-edit"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?=site_url('ta')?>">Tahun Anggaran</a></li>
              <li><a href="<?=site_url('pptk')?>">PPTK</a></li>
            </ul>
          </li>
          <?php } ?>
          <li><a href="<?=site_url('anggaran')?>"><i class="fa fa-bar-chart"></i>Anggaran</a></li>
          <li><a href="<?=site_url('realisasi')?>"><i class="fa fa-bar-chart"></i>Realiasi</a></li>
          <li><a href="<?=site_url('laporan')?>"><i class="fa fa-print"></i>Laporan</a></li>
          <li><a href="<?=site_url('login/logout')?>"><i class="fa fa-power-off"></i>Keluar</a></li>

        </ul>
      </div>


    </div>
    <!-- /sidebar menu -->
  </div>
</div>
