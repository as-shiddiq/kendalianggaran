<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Master Belanja</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Master Belanja</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_belanja' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_belanja',$where)->row();
                $id_belanja = $row->id_belanja;
								$kode_belanja = $row->kode_belanja;
								$nama_belanja = $row->nama_belanja;
								$tanggal_belanja = $row->tanggal_belanja;
              }
              else{
                $parameter='tambah';
                $id_belanja = '';
								$kode_belanja = '';
								$nama_belanja = '';
								$tanggal_belanja = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>belanja/<?=$parameter?>">
						<?php echo input_hidden('id_belanja',$id_belanja,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>kode belanja</label>
								<?php echo input_text('kode_belanja',$kode_belanja,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-6 full">
							<div class="form-group">
								<label>nama belanja</label>
								<?php echo textarea('nama_belanja',$nama_belanja,'md-input','required');?>
							</div>
						</div>

						<?php echo input_hidden('tanggal_belanja',$tanggal_belanja,'','required');?>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Master Belanja</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Master Belanja</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>belanja?tambah" class="btn btn-success" ns-click="true" ns-title="Data Master Belanja"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
