<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Pegawai</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Pegawai</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_pegawai' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_pegawai',$where)->row();
                $id_pegawai = $row->id_pegawai;
								$nip_pegawai = $row->nip_pegawai;
								$nama_pegawai = $row->nama_pegawai;
								$no_hp = $row->no_hp;
              }
              else{
                $parameter='tambah';
                $id_pegawai = '';
								$nip_pegawai = '';
								$nama_pegawai = '';
								$no_hp = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>pegawai/<?=$parameter?>">
						<?php echo input_hidden('id_pegawai',$id_pegawai,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nip pegawai [<small class="text-gray">Contoh : 197008012010011001</small>]</label>
								<?php echo input_number('nip_pegawai',$nip_pegawai,'md-input','required maxlength="18" minlength="18"');?>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group full">
								<label>nama pegawai</label>
								<?php echo input_text('nama_pegawai',$nama_pegawai,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>no hp</label>
								<?php echo input_number('no_hp',$no_hp,'md-input','required maxlength="14" minlength="10"');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Pegawai</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Pegawai</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>pegawai?tambah" class="btn btn-success" ns-click="true" ns-title="Data Pegawai"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
