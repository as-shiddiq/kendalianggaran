<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data PPTK</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data PPTK</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_pptk' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_pptk',$where)->row();
                $id_pptk = $row->id_pptk;
								$id_pegawai = $row->id_pegawai;
								$id_bidang = $row->id_bidang;
								$id_ta = $row->id_ta;
								$jenis_pptk = $row->jenis_pptk;
								$tanggal_pptk = $row->tanggal_pptk;
              }
              else{
                $parameter='tambah';
                $id_pptk = '';
								$id_pegawai = '';
								$id_bidang = '';
								$jenis_pptk = '';
								$tanggal_pptk = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>pptk/<?=$parameter?>">
             <?php echo input_hidden('id_pptk',$id_pptk,'','required');?>
						<?php echo input_hidden('id_ta',$id_ta,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nama pegawai</label>
								<?php
									$op=NULL;
                  $op['']='Pilih Salah Satu';
                  $this->db->order_by('nip_pegawai','ASC');
                  if($parameter=='ubah'){
                    $this->db->where('id_pegawai NOT IN(SELECT id_pegawai FROM tb_pptk WHERE id_ta='.$id_ta.' AND id_pegawai !='.$id_pegawai.')');
                  }
                  else{
                    $this->db->where('id_pegawai NOT IN(SELECT id_pegawai FROM tb_pptk WHERE id_ta='.$id_ta.')');
                  }
									$data=$this->db->get('tb_pegawai');
									foreach($data->result() as $row){
										$op[$row->id_pegawai]=$row->nip_pegawai.' - '.$row->nama_pegawai;
									}
									echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>bidang</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('nama_bidang','ASC');
									$data=$this->db->get('tb_bidang');
									foreach($data->result() as $row){
										$op[$row->id_bidang]=$row->nama_bidang;
									}
									echo select('id_bidang',$op,$id_bidang,'','required data-md-selectize');?>
							</div>
						</div>
						<!-- <div class="col-lg-12">
							<div class="form-group">
								<label>jenis pptk</label> -->
								<?php echo input_hidden('jenis_pptk',$jenis_pptk,'md-input','required');?>
							<!-- </div>
						</div> -->
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data PPTK</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data PPTK</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>pptk?tambah" class="btn btn-success" ns-click="true" ns-title="Data PPTK"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
