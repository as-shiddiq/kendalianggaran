-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2017 at 12:53 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kendalianggaran`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggaran`
--

CREATE TABLE `tb_anggaran` (
  `id_anggaran` int(12) NOT NULL,
  `id_pegawai` int(12) NOT NULL,
  `id_kegiatan` int(12) NOT NULL,
  `id_ta` int(11) NOT NULL,
  `pagu_anggaran` double(10,2) NOT NULL,
  `tanggal_anggaran` date NOT NULL,
  `id_belanja` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_anggaran`
--

INSERT INTO `tb_anggaran` (`id_anggaran`, `id_pegawai`, `id_kegiatan`, `id_ta`, `pagu_anggaran`, `tanggal_anggaran`, `id_belanja`) VALUES
(1, 1, 2, 1, 10000.00, '2017-06-28', 1),
(2, 2, 1, 1, 121212.00, '2017-06-28', 2),
(3, 2, 3, 1, 1000000.00, '2017-07-04', 2),
(4, 1, 4, 1, 100000.00, '2017-07-04', 1),
(5, 1, 2, 1, 12121212.00, '2017-07-04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_belanja`
--

CREATE TABLE `tb_belanja` (
  `id_belanja` int(11) NOT NULL,
  `kode_belanja` varchar(100) NOT NULL,
  `nama_belanja` text NOT NULL,
  `tanggal_belanja` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_belanja`
--

INSERT INTO `tb_belanja` (`id_belanja`, `kode_belanja`, `nama_belanja`, `tanggal_belanja`) VALUES
(1, '09', 'Alat Tulis Kantor', '2017-06-28'),
(2, '080', 'Belanja Materai\r\n', '2017-06-28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bidang`
--

CREATE TABLE `tb_bidang` (
  `id_bidang` int(12) NOT NULL,
  `nama_bidang` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bidang`
--

INSERT INTO `tb_bidang` (`id_bidang`, `nama_bidang`) VALUES
(2, 'Bidang BKPSDM2'),
(3, 'Pengendalian SDM'),
(4, 'Bidang BKPSDM3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id_kegiatan` int(12) NOT NULL,
  `kode_kegiatan` varchar(200) NOT NULL,
  `nama_kegiatan` text NOT NULL,
  `id_program` int(12) NOT NULL,
  `tanggal_kegiatan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id_kegiatan`, `kode_kegiatan`, `nama_kegiatan`, `id_program`, `tanggal_kegiatan`) VALUES
(1, '09', 'kegaiatan 1a', 1, '2017-07-03'),
(2, '09', 'kegiatan 1', 1, '2017-07-03'),
(3, '09', 'Kegiatan 2a', 2, '2017-07-04'),
(4, '10', 'kegiatan 2b', 2, '2017-07-04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `id_pegawai` int(12) NOT NULL,
  `nip_pegawai` varchar(25) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `no_hp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id_pegawai`, `nip_pegawai`, `nama_pegawai`, `no_hp`) VALUES
(1, '109900909090', 'Badarudin', '10291201921029'),
(2, '199901011010101001', 'pegawai2', '0898989898'),
(3, '090900909090909090', 'pegawai 1', '10912019210291');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengguna`
--

CREATE TABLE `tb_pengguna` (
  `id_pengguna` int(12) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `id_pegawai` int(12) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengguna`
--

INSERT INTO `tb_pengguna` (`id_pengguna`, `username`, `password`, `nama`, `id_pegawai`, `level`) VALUES
(1, 'admin', '123456', 'administrator', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pptk`
--

CREATE TABLE `tb_pptk` (
  `id_pptk` int(12) NOT NULL,
  `id_pegawai` int(12) NOT NULL,
  `id_bidang` int(12) NOT NULL,
  `id_ta` int(11) NOT NULL,
  `jenis_pptk` varchar(200) NOT NULL,
  `tanggal_pptk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pptk`
--

INSERT INTO `tb_pptk` (`id_pptk`, `id_pegawai`, `id_bidang`, `id_ta`, `jenis_pptk`, `tanggal_pptk`) VALUES
(1, 1, 2, 1, 'tidak ada njir', '2017-06-06'),
(2, 2, 3, 1, '', '2017-06-28'),
(3, 3, 4, 1, '', '2017-07-04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_program`
--

CREATE TABLE `tb_program` (
  `id_program` int(12) NOT NULL,
  `kode_program` varchar(100) NOT NULL,
  `nama_program` text NOT NULL,
  `tanggal_program` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_program`
--

INSERT INTO `tb_program` (`id_program`, `kode_program`, `nama_program`, `tanggal_program`) VALUES
(1, '05', 'program 1', '2017-07-03'),
(2, '06', 'Program 2', '2017-07-04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_realisasi`
--

CREATE TABLE `tb_realisasi` (
  `id_realisasi` int(12) NOT NULL,
  `id_anggaran` int(12) NOT NULL,
  `id_pengguna` int(12) NOT NULL,
  `id_ta` int(11) NOT NULL,
  `realisasi` double(10,2) NOT NULL,
  `tanggal_realisasi` date NOT NULL,
  `tanggal_simpan` date NOT NULL,
  `status` enum('Realisasi','Talangan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_realisasi`
--

INSERT INTO `tb_realisasi` (`id_realisasi`, `id_anggaran`, `id_pengguna`, `id_ta`, `realisasi`, `tanggal_realisasi`, `tanggal_simpan`, `status`) VALUES
(1, 1, 1, 1, 10000.00, '2017-07-04', '2017-07-04', 'Realisasi'),
(3, 4, 1, 1, 10000.00, '2017-07-04', '2017-07-04', 'Realisasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ta`
--

CREATE TABLE `tb_ta` (
  `id_ta` int(11) NOT NULL,
  `nama_ta` varchar(4) NOT NULL,
  `aktif` enum('Y','T') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ta`
--

INSERT INTO `tb_ta` (`id_ta`, `nama_ta`, `aktif`) VALUES
(1, '2017', 'Y'),
(2, '2019', 'T');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anggaran`
--
ALTER TABLE `tb_anggaran`
  ADD PRIMARY KEY (`id_anggaran`);

--
-- Indexes for table `tb_belanja`
--
ALTER TABLE `tb_belanja`
  ADD PRIMARY KEY (`id_belanja`);

--
-- Indexes for table `tb_bidang`
--
ALTER TABLE `tb_bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tb_pengguna`
--
ALTER TABLE `tb_pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `tb_pptk`
--
ALTER TABLE `tb_pptk`
  ADD PRIMARY KEY (`id_pptk`);

--
-- Indexes for table `tb_program`
--
ALTER TABLE `tb_program`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `tb_realisasi`
--
ALTER TABLE `tb_realisasi`
  ADD PRIMARY KEY (`id_realisasi`);

--
-- Indexes for table `tb_ta`
--
ALTER TABLE `tb_ta`
  ADD PRIMARY KEY (`id_ta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anggaran`
--
ALTER TABLE `tb_anggaran`
  MODIFY `id_anggaran` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_belanja`
--
ALTER TABLE `tb_belanja`
  MODIFY `id_belanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_bidang`
--
ALTER TABLE `tb_bidang`
  MODIFY `id_bidang` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `id_kegiatan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  MODIFY `id_pegawai` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pengguna`
--
ALTER TABLE `tb_pengguna`
  MODIFY `id_pengguna` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_pptk`
--
ALTER TABLE `tb_pptk`
  MODIFY `id_pptk` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_program`
--
ALTER TABLE `tb_program`
  MODIFY `id_program` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_realisasi`
--
ALTER TABLE `tb_realisasi`
  MODIFY `id_realisasi` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_ta`
--
ALTER TABLE `tb_ta`
  MODIFY `id_ta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
